# DevopsInNet::Cognito::CustomDomain

A custom resource to fetch Cognito Identity Pool CloudFrontDistribution CNAME

## Syntax

To declare this entity in your AWS CloudFormation template, use the following syntax:

### JSON

<pre>
{
    "Type" : "DevopsInNet::Cognito::CustomDomain",
    "Properties" : {
        "<a href="#cognitouserpoolid" title="CognitoUserPoolId">CognitoUserPoolId</a>" : <i>String</i>,
        "<a href="#cloudfrontdistributioncname" title="CloudFrontDistributionCname">CloudFrontDistributionCname</a>" : <i>String</i>
    }
}
</pre>

### YAML

<pre>
Type: DevopsInNet::Cognito::CustomDomain
Properties:
    <a href="#cognitouserpoolid" title="CognitoUserPoolId">CognitoUserPoolId</a>: <i>String</i>
    <a href="#cloudfrontdistributioncname" title="CloudFrontDistributionCname">CloudFrontDistributionCname</a>: <i>String</i>
</pre>

## Properties

#### CognitoUserPoolId

The id of the Cognito User Pool

_Required_: Yes

_Type_: String

_Pattern_: <code>[\w-]+_[0-9a-zA-Z]+</code>

_Update requires_: [No interruption](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-updating-stacks-update-behaviors.html#update-no-interrupt)

#### CloudFrontDistributionCname

Cloudfront Distribution Cname for CustomDomain

_Required_: No

_Type_: String

_Update requires_: [No interruption](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-updating-stacks-update-behaviors.html#update-no-interrupt)

## Return Values

### Ref

When you pass the logical ID of this resource to the intrinsic `Ref` function, Ref returns the CloudFrontDistributionCname.
