package resource

import (
	"errors"
	"fmt"
	"github.com/aws-cloudformation/cloudformation-cli-go-plugin/cfn/handler"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"log"
)

func fetchCloudFrontCname(sess *session.Session, cognitoUserPoolId *string) (cloudfrontCname string, err error) {
	log.Println("Fetching information about CognitoUserPool id:", *cognitoUserPoolId)

	svc := cognitoidentityprovider.New(sess)
	describeUserPoolInput := cognitoidentityprovider.DescribeUserPoolInput{
		UserPoolId: cognitoUserPoolId,
	}

	describeUserPool, err := svc.DescribeUserPool(&describeUserPoolInput)
	if err != nil {
		return "", err

	}
	domainName := describeUserPool.UserPool.CustomDomain
	describeUserPoolDomainInput := cognitoidentityprovider.DescribeUserPoolDomainInput{
		Domain: domainName,
	}
	describeDomain, err := svc.DescribeUserPoolDomain(&describeUserPoolDomainInput)
	if err != nil {
		fmt.Println(err)
		return "", err

	}

	cloudFrontDistribution := describeDomain.DomainDescription.CloudFrontDistribution

	return *cloudFrontDistribution, nil
}

// Create handles the Create event from the Cloudformation service.
func Create(req handler.Request, prevModel *Model, currentModel *Model) (handler.ProgressEvent, error) {
	cloudFrontDistribution, err := fetchCloudFrontCname(req.Session, currentModel.CognitoUserPoolId)

	if err != nil {
		return handler.ProgressEvent{}, errors.New(err.Error())
	}

	currentModel.CloudFrontDistributionCname = &cloudFrontDistribution

	response := handler.ProgressEvent{
		OperationStatus: handler.Success,
		Message:         "List complete",
		ResourceModel:   currentModel,
	}

	return response, nil
}

// Read handles the Read event from the Cloudformation service.
func Read(req handler.Request, prevModel *Model, currentModel *Model) (handler.ProgressEvent, error) {
	cloudFrontDistribution, err := fetchCloudFrontCname(req.Session, currentModel.CognitoUserPoolId)

	if err != nil {
		return handler.ProgressEvent{}, errors.New(err.Error())
	}

	currentModel.CloudFrontDistributionCname = &cloudFrontDistribution

	response := handler.ProgressEvent{
		OperationStatus: handler.Success,
		Message:         "Create complete",
		ResourceModel:   currentModel,
	}

	return response, nil
}

// Update handles the Update event from the Cloudformation service.
func Update(req handler.Request, prevModel *Model, currentModel *Model) (handler.ProgressEvent, error) {

	cloudFrontDistribution, err := fetchCloudFrontCname(req.Session, currentModel.CognitoUserPoolId)

	if err != nil {
		return handler.ProgressEvent{}, errors.New(err.Error())
	}

	currentModel.CloudFrontDistributionCname = &cloudFrontDistribution

	response := handler.ProgressEvent{
		OperationStatus: handler.Success,
		Message:         "Update complete",
		ResourceModel:   currentModel,
	}

	return response, nil
}

// Delete handles the Delete event from the Cloudformation service.
func Delete(req handler.Request, prevModel *Model, currentModel *Model) (handler.ProgressEvent, error) {

	response := handler.ProgressEvent{
		OperationStatus: handler.Success,
		Message:         "Delete complete",
		ResourceModel:   currentModel,
	}

	return response, nil
}

// List handles the List event from the Cloudformation service.
func List(req handler.Request, prevModel *Model, currentModel *Model) (handler.ProgressEvent, error) {
	// Add your code here:
	// * Make API calls (use req.Session)
	// * Mutate the model
	// * Check/set any callback context (req.CallbackContext / response.CallbackContext)

	/*
	   // Construct a new handler.ProgressEvent and return it
	   response := handler.ProgressEvent{
	       OperationStatus: handler.Success,
	       Message: "List complete",
	       ResourceModel: currentModel,
	   }

	   return response, nil
	*/

	// Not implemented, return an empty handler.ProgressEvent
	// and an error

	cloudFrontDistribution, err := fetchCloudFrontCname(req.Session, currentModel.CognitoUserPoolId)

	if err != nil {
		return handler.ProgressEvent{}, errors.New(err.Error())
	}

	currentModel.CloudFrontDistributionCname = &cloudFrontDistribution

	response := handler.ProgressEvent{
		OperationStatus: handler.Success,
		Message:         "List complete",
		ResourceModel:   currentModel,
	}

	return response, nil
}
